﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Jarvis01
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Jarvis01());
        }
    }
}
