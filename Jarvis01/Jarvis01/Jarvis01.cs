﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Speech;
using System.Speech.Synthesis;

namespace Jarvis01
{
    public partial class Jarvis01 : Form
    {
        public Jarvis01()
        {
            InitializeComponent();
        }
        SpeechSynthesizer Speech;
        private void Jarvis01_Load(object sender, EventArgs e)
        {
            Speech = new SpeechSynthesizer();
            buttonResume.Enabled = false;
            buttonPause.Enabled = false;
            buttonStop.Enabled = false;
        }

        private void ButtonSpeak_Click(object sender, EventArgs e)
        {
            //Disposes the SpeechSynthesizer object   
            Speech.Dispose();
            if (richTextBox.Text != "")
            {
                Speech = new SpeechSynthesizer();
                //Asynchronously speaks the contents present in RichTextBox1   
                Speech.SpeakAsync(richTextBox.Text);
                buttonPause.Enabled = true;
                buttonStop.Enabled = true;
            }
        }

        private void ButtonResume_Click(object sender, EventArgs e)
        {
            if (Speech != null)
            {
                if (Speech.State == SynthesizerState.Paused)
                {
                    //Resumes the SpeechSynthesizer object after it has been paused.   
                    Speech.Resume();
                    buttonResume.Enabled = false;
                    buttonSpeak.Enabled = true;
                }
            }
        }

        private void ButtonPause_Click(object sender, EventArgs e)
        {
            if (Speech != null)
            {
                //Gets the current speaking state of the SpeechSynthesizer object.   
                if (Speech.State == SynthesizerState.Speaking)
                {
                    //Pauses the SpeechSynthesizer object.   
                    Speech.Pause();
                    buttonResume.Enabled = true;
                    buttonSpeak.Enabled = false;
                }
            }
        }

        private void ButtonStop_Click(object sender, EventArgs e)
        {
            if (Speech != null)
            {
                //Disposes the SpeechSynthesizer object   
                Speech.Dispose();
                buttonSpeak.Enabled = true;
                buttonResume.Enabled = false;
                buttonPause.Enabled = false;
                buttonStop.Enabled = false;
            }
        }
    }
}
